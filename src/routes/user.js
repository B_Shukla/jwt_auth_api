const express = require('express')
const UserController = require('../controller/UserController')
const auth = require('../middleware/auth')

const router = express.Router()
const userController = new UserController()

router.post('/createUser',userController.createUser)
router.get('/login',userController.login)
router.get('/profile',auth,userController.check)

module.exports = router