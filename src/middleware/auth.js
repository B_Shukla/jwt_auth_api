const jwt = require('jsonwebtoken')
const UserController = require('../controller/UserController')
const config = require('../connection/config')

const userController = new UserController()

module.exports = function (req, res, next) {
    if (req.header("Authorization")) {
        const authHeader = req.header("Authorization").replace('Bearer ', '');
        const token = authHeader;
        const username = req.body.username
        try {
            const decoder = jwt.verify(token, config.JWT.JWT_ENCRYPTION)
            const user = userController.findUser(username, token)
            if (user) {

                console.log(user)
                req.user = user
                next()
            }
            else {
                throw new Error()
            }

        } catch (e) {
            res.status(400).send('token expired')
        }
    }
    else {
        res.status(400).send('header not present')
    }

}