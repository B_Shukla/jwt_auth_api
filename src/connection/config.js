module.exports = {
    JWT: {
        JWT_ENCRYPTION: process.env.JWT_ENCRYPTION || "jwtLearning",
        JWT_EXPIRATION: process.env.JWT_EXPIRATION || "60s"
    }
};