const chalk = require('chalk')
const bcrypt = require('bcrypt')
const fs = require('fs')
const jwt = require('jsonwebtoken')
const config = require('../connection/config')

module.exports = class UserController {
    check(req, res) {
        console.log(chalk.green.inverse.bold('success'))
        res.send('hello user')
    }
    async login(req, res) {
        //console.log(req)
        console.log(req.body)
        const username = req.body.username
        const password = req.body.password
        const allUsers = loadUser()
        const authorisedUser = allUsers.filter((user) => {
            if (user.username == username) {
                return user
            }
        })[0]

        if (authorisedUser) {
            const hashpwd = authorisedUser.password
            const isMatch = bcrypt.compare(password, hashpwd)
            if (isMatch) {
                const getToken = await generateAuthToken(req.body.username)
                authorisedUser.token = getToken
                allUsers.filter((user) => {
                    if (user.username == username) {
                        user.token = getToken
                    }
                })
                fs.writeFileSync('src/data/userList.json', JSON.stringify(allUsers))
                res.status(200).send({ authorisedUser, msg: 'Logged In!!!' })
            }
            else {
                res.status(400).send('password does not match.Please check')
            }
        }
        else {
            res.status(400).send('username does not exist')
        }
    }

    async createUser(req, res) {
        console.log(chalk.blue.inverse.bold('createUser called'))
        if (req.body.username && req.body.password) {
            const allUsers = loadUser();

            const duplicate = allUsers.filter((user) => {
                return user.username == req.body.username
            })

            if (duplicate.length === 0) {
                const hashpwd = await bcrypt.hash(req.body.password, 8)
                const getToken = await generateAuthToken(req.body.username)
                console.log(getToken)
                const user = {
                    username: req.body.username,
                    password: hashpwd,
                    token: getToken
                }

                try {
                    allUsers.push(user)
                    fs.writeFileSync('src/data/userList.json', JSON.stringify(allUsers))
                    res.status(200).send({ user, msg: "successfully created" })
                } catch (e) {
                    res.status(500).send(e)
                }
            }
            else {
                res.status(400).send('error: username already exist')
            }

        }
        else {
            res.status(400).send("error: please provide username and password")
        }

    }

    findUser(username,token) {
        const allUsers = loadUser();
    
        const user = allUsers.filter((user) => {
            if (user.username == username && user.token == token) {
                return user
            }
        })[0]
    
        if (user) {
            return user
        }
        else {
            return null
        }
    }

}

function loadUser() {
    try {
        const dataBuffer = fs.readFileSync('src/data/userList.json')
        const dataJson = dataBuffer.toString()
        return JSON.parse(dataJson)
    } catch (error) {
        return []
    }
}


async function generateAuthToken(authorisedUser) {
    const token = jwt.sign({ username: authorisedUser.username }, config.JWT.JWT_ENCRYPTION, { expiresIn: config.JWT.JWT_EXPIRATION })
    return token
}